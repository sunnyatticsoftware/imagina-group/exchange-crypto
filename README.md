# exchange-crypto

Ejercicio con API REST para testear que una calculadora nos devuelve el equivalente de una cryptomoneda en moneda fiat.

Se utiliza para conocer mejor xUnit, FluentAssertion, Moq y TestServer.

Dada la siguiente calculadora
```
public class CryptoCalculator
{
    private readonly IDictionary<string, double> _ratesInEur;

    public CryptoCalculator(IDictionary<string, double> ratesInEur)
    {
        _ratesInEur = ratesInEur;
    }

    public double ToEur(string cryptoCode, double cryptoAmount)
    {
        var isCryptoSupported = _ratesInEur.TryGetValue(cryptoCode, out var rateInEuro);
        if (!isCryptoSupported)
        {
            throw new CryptoNotSupportedException(cryptoCode);
        }

        var result = cryptoAmount * rateInEuro;
        return result;
    }
}

public class CryptoNotSupportedException
    : Exception
{
    public CryptoNotSupportedException(string cryptoCode)
        : base($"Unsupported crypto {cryptoCode}")
    {
    }
}
```

1- Crea tests unitarios para probar algunos escenarios. Utiliza xUnit, FluentAssertions y Moq.
2- Crea una API REST para exponer esta funcionalidad por http en `GET http://localhost:4000/api/cryptoCurrencies/HBAR/calculations?cryptoAmount=300`
3- Testea la respuesta a una petición GET en ese endpoint. Utiliza la librería TestServer para ello.